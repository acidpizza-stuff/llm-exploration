from transformers import AutoTokenizer, AutoModelForCausalLM, pipeline

from langchain.document_loaders.csv_loader import CSVLoader
from langchain.text_splitter import CharacterTextSplitter
from langchain.embeddings.sentence_transformer import SentenceTransformerEmbeddings
from langchain.prompts import PromptTemplate
from langchain.chains import RetrievalQA

from langchain_community.llms.huggingface_pipeline import HuggingFacePipeline
from langchain_community.vectorstores import Chroma


model_id = 'alokabhishek/Llama-2-7b-chat-hf-bnb-4bit'

template = (
    'You are a helpful assistant that provides accurate and concise responses. Answer the question using the data context above. Keep your answer grounded in the facts of the data. If the data doesn’t contain the facts to answer the question, just say "I dont know.".\n'
    '\n'
    '{context}\n'
    '\n'
    'Question: {question}\n'
    '\n'
    'Answer:'
)

def run_rag():
    # load model tokenizer
    tokenizer = AutoTokenizer.from_pretrained(model_id)
    # create pipeline
    model = AutoModelForCausalLM.from_pretrained(model_id, low_cpu_mem_usage=True)
    text_generator_pipeline = pipeline(task='text-generation', model=model, tokenizer=tokenizer)
    # use HuggingFace Pipeline wrapper
    llm = HuggingFacePipeline(pipeline=text_generator_pipeline, model_kwargs={'temperature':0})

    # Load documents as CSV
    loader = CSVLoader('data.csv')
    docs = loader.load()
    
    # split document into text chunks
    text_splitter = CharacterTextSplitter(chunk_size=1000, chunk_overlap=0)
    docs = text_splitter.split_documents(docs)

    # create embedding function
    embedding_function = SentenceTransformerEmbeddings(model_name="all-MiniLM-L6-v2")

    # Load into ChromaDB
    db = Chroma.from_documents(docs, embedding_function)

    # Format prompt
    prompt = PromptTemplate(template=template, input_variables=["context", "question"])
    chain = RetrievalQA.from_chain_type(
        llm=llm,
        chain_type='stuff',
        retriever=db.as_retriever(search_kwargs={'k': 1}),
        chain_type_kwargs={'prompt': prompt},
    )

    terminate = False
    while not terminate:
        query = input("Query (type 'end' to terminate):" )
        
        if query == "end":
            terminate = True

        elif query == "":
            pass

        else:
            response = chain.invoke(query)
            print(response['result'])


run_rag()