import transformers
from transformers import AutoModelForCausalLM, AutoTokenizer, pipeline

model_id = 'alokabhishek/Llama-2-7b-chat-hf-bnb-4bit'

def format_prompt_llama2(prompt):
    system_prompt = 'You are a helpful assistant that provides accurate and concise responses'
    return (
        f'[INST] <<SYS>>\n'
        f'{system_prompt}\n'
        f'<</SYS>>\n\n'
        f'{prompt.strip()} [/INST]'
    )


def run_model_manually(prompt):
    # load model tokenizer
    tokenizer = AutoTokenizer.from_pretrained(model_id)
    # load model on gpu
    model = AutoModelForCausalLM.from_pretrained(model_id, device_map='auto')

    # format prompt
    formatted_prompt = format_prompt_llama2(prompt)
    # tokenize inputs
    inputs = tokenizer(formatted_prompt, return_tensors='pt').to('cuda')
    # generate outputs
    outputs = model.generate(**inputs, max_new_tokens=1024)
    # decode output
    response = tokenizer.decode(outputs[0])
    
    print(response)


def run_model_pipeline(prompt):
    # load model tokenizer
    tokenizer = AutoTokenizer.from_pretrained(model_id)
    # create pipeline
    text_generator_pipeline = pipeline(task='text-generation', model=model_id, tokenizer=tokenizer)

    # format prompt
    formatted_prompt = format_prompt_llama2(prompt)
    # run pipeline to generate outputs
    outputs = text_generator_pipeline(formatted_prompt, max_new_tokens=1024)

    print(outputs[0]['generated_text'])


# run_model_manually('count to 10')

run_model_pipeline('count to 10')
